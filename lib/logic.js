function onNewBloodSample(NewBloodSample) {
    NewBloodSample.bloodsample.BloodSampleId = NewBloodSample.newBloodSampleId;
    NewBloodSample.bloodsample.BloodSampleDate =  NewBloodSample.newBloodSampleDate;
    NewBloodSample.bloodsample.SyringeId = NewBloodSample.newSyringeId;

    return getAssetRegistry('org.acme.blood.BloodSample')
    .then(function (assetRegistry) {
        return assetRegistry.update(NewBloodSample.BloodSample);
    });
}
    
function onNewVehicule(NewVehicule){
    NewVehicule.vehicule.VehiculeName = NewVehicule.newVehiculeName;
    NewVehicule.vehicule.VehiculeImmatriculation = NewVehicule.newVehiculeImmatriculation;
    NewVehicule.vehicule.VehiculeMiles = NewVehicule.newVehiculeMiles;
    
    return getAssetRegistry('org.acme.blood.Vehicule')
        .then(function (assetRegistry) {
            return assetRegistry.update(NewVehicule.vehicule);
        });
}

function onNewBloodSamplePlace(NewBloodSamplePlace){
    NewBloodSamplePlace.bloodsampleplace.BloodSampleId = NewBloodSamplePlace.newBloodSampleId;
    NewBloodSamplePlace.bloodsampleplace.BloodSampleCity = NewBloodSamplePlace.newBloodSampleCity;
    NewBloodSamplePlace.bloodsampleplace.BloodSampleTemperature = NewBloodSamplePlace.newBloodSampleTemperature;
    NewBloodSamplePlace.bloodsampleplace.BloodSampleCityDate = NewBloodSamplePlace.newBloodSampleCityDate;

    return getAssetRegistry('org.acme.blood.BloodSamplePlace')
    .then(function (assetRegistry) {
        return assetRegistry.update(NewBloodSamplePlace.bloodsampleplace);
    });
}

function onNewSensor(NewSensor){
    NewSensor.sensor.SensorTemperature = NewSensor.newSensorTemperature;

    return getAssetRegistry('org.acme.blood.Sensor')
    .then(function (assetRegistry) {
        return assetRegistry.update(NewSensor.sensor);
    });
}

